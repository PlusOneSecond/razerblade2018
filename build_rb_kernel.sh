#!/bin/bash
# vars
kernel_version=4.17.11
kernel_url=https://cdn.kernel.org/pub/linux/kernel/v4.x/linux-${kernel_version}.tar.xz
workingDir=/tmp/build-rbkernel-$(date +%Y%m%d)
kernel_zip=${kernel_url##*/}
kernel_dir="${workingDir}/${kernel_zip%%.tar.xz}"
kernel_config=/boot/config-$(uname -r)
patch_p1=https://raw.githubusercontent.com/jbdrthapa/razerblade15/master/razerfiles/touchpad/gpiolib.patch
patch_p1_target=drivers/gpio/gpiolib.c
patch_p2=https://raw.githubusercontent.com/jbdrthapa/razerblade15/master/razerfiles/touchpad/pinctrl-intel.patch
patch_p2_target=drivers/pinctrl/intel/pinctrl-intel.c
prereqs="git curl build-essential kernel-package fakeroot libncurses5-dev libssl-dev ccache flex bison"
debs=(linux-headers-${kernel_version}-custom_${kernel_version}-custom-1_amd64.deb linux-image-${kernel_version}-custom_${kernel_version}-custom-1_amd64.deb linux-image-${kernel_version}-custom-dbg_${kernel_version}-custom-1_amd64.deb linux-libc-dev_${kernel_version}-custom-1_amd64.deb)
# runtime
Fail () {
	echo "$1"
	exit 99
}
if [ -d "$workingDir" ]; then
  echo "Cleaning up old build"
  rm -rf "${workingDir:?}"/* || Fail "Couldn't cleanup $workingDir, permissions?"
else
  echo "Creating workspace $workingDir"
  mkdir "$workingDir" || Fail "Couldn't create $workingDir, permissions?"
fi
cd "$workingDir" || Fail "Couldn't navigate to $workingDir, permisisons?"
echo "Installing required packages"
sudo apt-get install ${prereqs} || Fail "Couldn't install the required packages: $prereqs"  #shellcheck disable=SC2086
[ -f $kernel_zip ] && rm $kernel_zip #cleanup old
echo "Downloading $kernel_url"
curl -sO $kernel_url || Fail "The download of $kernel_url seems to have Failed"
[ -f ${kernel_zip} ] || Fail "Didn't find ${kernel_zip} in $workingDir, check the download"
echo "Untarring $kernel_zip"
tar xvf ${kernel_zip} || Fail "Couldn't untar $kernel_zip, wrong format?"
cd "${kernel_dir}" || Fail "Couldn't navigate to $kernel_dir, did the untar Fail?"
echo "Applying patch 1"
curl -sO $patch_p1 || Fail "Download of $patch_p1 failed"
patch $patch_p1_target < ${patch_p1##*/} || Fail "Patch 1 failed, probably ask someone"
echo "Applying patch 2"
curl -sO $patch_p2 || Fail "Download of $patch_p2 failed"
patch $patch_p2_target < ${patch_p2##*/} || Fail "Patch 2 failed, probably ask someone"
cp "$kernel_config" .config || Fail "Couldn't copy the current kernel config"
make clean || Fail "Couldn't clean up the current kernel"
echo "Building the kernel.  This could take a while."
build_cmd="make -j "$(getconf _NPROCESSORS_ONLN)" deb-pkg LOCALVERSION=-custom"
yes "" | $build_cmd || Fail "Failed while building the kernel, probably ask for help"
cd ../ || Fail "This will never happen unless something external is messing with us or theres a full disk or something"
for deb in ${debs[@]}; do
	sudo dpkg -i "$deb" || Fail "Failed while installing the new kernel, probably ask someone"
done
echo "Kernel build done, reboot to pick it up"
