# Running Ubuntu on Razer 2018

This is me installing Ubunut 18.04.1 onto a new Razer Blade 15.6" 2018.  I do plenty of gaming, so getting Steam working is one of my major goals, and one of my projects is getting GPU Passthrough working as well.  
I've seen a lot of suggestions to turn off fast boot and secure boot.  I didn't want to have to do those things so I've been trying to solve those problems, rather than just work around them.  I appreciate the added security that secure boot provides.

## Installation

- I have TPM/Secure Boot turned on
- Full disk encryption w/LVM (wiped the drive)
- Default the rest of it

### Switch to proprietary Nvidia drivers and disable Wayland

add the nvidia drivers ppa

```bash
sudo add-apt-repository ppa:graphics-drivers/ppa
sudo apt-get update
sudo ubuntu-drivers autoinstall
```

for secure boot we need to sign our new drivers

```bash
KEYDIR=~/keys
DRIVERS=(nvidia-drm.ko  nvidia.ko  nvidia-modeset.ko  nvidia-uvm.ko)
mkdir $KEYDIR
cd $KEYDIR
openssl req -new -x509 -newkey rsa:2048 -keyout MOK.priv -outform DER -out MOK.der -nodes -days 36500 -subj "/CN=Descriptive name/"
# consider securing your keys better than this
chmod 700 $KEYDIR
chmod 400 $KEYDIR/MOK.der
chmod 400 $KEYDIR/MOK.priv
# this next step will ask for a password that will need to be re-entered at the next boot
sudo mokutil --import $KEYDIR/MOK.der
for driver in ${DRIVERS[@]}; do
  sudo /usr/src/linux-headers-$(uname -r)/scripts/sign-file sha256 $KEYDIR/MOK.priv $KEYDIR/MOK.der /lib/modules/$(uname -r)/updates/dkms/$driver
done
```

edit `/etc/gdm3/custom.conf`
uncomment line 7 to disable Wayland

```diff
- #WaylandEnable=false
+ WaylandEnable=false
```

blacklist the nouveau drivers.  edit `/etc/modprobe.d/blacklist.conf` and add the following lines to the end of the file

```diff
+ blacklist nouveau
+ blacklist lbm-nouveau
+ options nouveau modeset=0
+ alias nouveau off
+ alias lbm-nouveau off
```

Then run

```bash
sudo update-initramfs -u
```

### Lets install Bumblebee

Bumblebee won't work with Vulkan (sadness) for now, but for all opengl we've got bumblebee.  Its the Linux equivilent of Optimus.

```bash
sudo apt install bumblebee bumblebee-nvidia
```

the defaults dont work so we need to tweak  
start by editing `/etc/bumblebee/bumblebee.conf`

```diff
- PMMethod=auto
+ PMMethod=bbswitch
- LibraryPath=/usr/lib/nvidia-current:/usr/lib32/nvidia-current
+ LibraryPath=/usr/lib/x86_64-linux-gnu:/usr/lib/i386-linux-gnu
- XorgModulePath=/usr/lib/nvidia-current/xorg,/usr/lib/xorg/modules
+ XorgModulePath=/usr/lib/x86_64-linux-gnu/nvidia/xorg,/usr/lib/xorg/modules,/usr/lib/xorg/modules/input
```

use the iGPU by default

```bash
sudo prime-select intel
```

we don't want nvidia to load by default, but we need to allow it to be loaded later  
the default blacklist causes problems, so we need to comment out a few lines  
edit `/etc/modprobe.d/blacklist-nvidia.conf`

```diff
- alias nvidia off
- alias nvidia-drm off
- alias nvidia-modeset off
+ #alias nvidia off
+ #alias nvidia-drm off
+ #alias nvidia-modeset off
```

Nvidia's libglvnd doesn't seem to be compatible so we need to disable it  
The easiest way to handle that seems to be to just add it permanently to  
all profiles
Just create `/etc/profile.d/disable_glvand.sh` with the following

```bash
export __GLVND_DISALLOW_PATCHING=1
```

Now reboot

```bash
sudo apt-get install mesa-utils
optirun glxinfo |grep -i vendor
# server glx vendor string: NVIDIA Corporation
```

#### Getting steam games to use the dGPU

You'll need to modify the runtime of games to get them to use bumblebee.  For each steam game you'll need to go into the properties and set the launch options to

```bash
optirun -b primus %command%
```

![launch properties on a steam game][steam_optirun]

## Fixing sleep problems

A couple people in the community noticed that the rb18 would wake up and immediately kick on the fans after being suspeneded.  I found [this page][sleep] that helped me figure out how to fix it.  As root, run the following

```bash
waking=$(acpitool -w |grep enabled |awk '{print $2}')
for dev in $waking; do
  echo "$dev" > /proc/acpi/wakeup
done
```

Now if you run `acpitool -w` you should see no enabled devices.

## Pretty Desktop

I love [Arc Dark][arc] theme for gtk and gnome-shell.

![Pretty themes](screenshot.png)

[Wallhaven][wallhaven] had some great backgrounds

![nice background](background.jpg)

![lovely lockscreen](lockscreen.jpg)

## Special notes

I relied pretty heavily on [this guide][dGPU]

[dGPU]: https://gist.github.com/Misairu-G/616f7b2756c488148b7309addc940b28 "GuThis is a guide for passing through you dGPU on your laptop for your VM"
[openrazer]: https://openrazer.github.io "Where snakes meet penguin"
[build_kernel]: build_rb_kernel.sh "get your trackpad on"
[steam_optirun]: optirun.png "steam on our dgpu"
[sleep]: https://theoryl1.wordpress.com/2016/06/18/ubuntu-wakes-up-immediately-after-suspend/ "fixing sleep issues"
[arc]: https://github.com/horst3180/arc-theme "pretty arc-dark theme"
[wallhaven]: https://alpha.wallhaven.cc/ "desktop backgrounds"